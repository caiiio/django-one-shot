from django.shortcuts import render, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


# Create your views here.
def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolist_list": todolists,
    }
    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    todolist = TodoList.objects.get(id=id)
    context = {
        "todolist": todolist,
    }

    return render(request, "todo_lists/detail.html", context)


def todo_list_create(request):
    form = TodoListForm()
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list_detail = form.save()

            return redirect("todo_list_detail", todo_list_detail.id)
    context = {
        "form": form
    }
    return render(request, "todo_lists/create.html", context)
